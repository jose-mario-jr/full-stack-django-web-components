import { App } from "./components/App.js"

const app = new App()
document.body.appendChild(app)

if(localStorage.getItem('auth')){
  app.switchView('/')
  app.switchView('/login')
} else {
  app.switchView('/login')
}