const template = document.getElementById("t-alerts")

class Alerts extends HTMLElement {
  constructor() {
    super()
    this.appendChild(template.content.cloneNode(true))
  }

  show(msg, type = "error", duration = 5000) {
    let newMsg = document.createElement("div")
    newMsg.textContent = msg
    newMsg.className = type
    newMsg.onclick = newMsg.remove
    this.appendChild(newMsg)
    setTimeout(newMsg.remove.bind(newMsg), duration)
  }
}

customElements.define("alerts-component", Alerts)

export { Alerts }
