import { Form } from "./Form.js"
import { Leads } from "./Leads.js"

const template = document.getElementById("t-dashboard")

class Dashboard extends HTMLElement {
  constructor() {
    super()
    this.appendChild(template.content.cloneNode(true))
  }
  updateLeads() {
    this.querySelector("leads-component").update()
  }
}

customElements.define("dashboard-component", Dashboard)

export { Dashboard }
