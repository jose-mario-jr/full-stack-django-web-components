import { errorMessage, successMessage } from "./messages.js"

export const getLeads = async () => {
  let res = await fetch("/api/leads/")
  if (res.status == 200) {
    let data = await res.json()
    return data
  }else if(res.status == 403){
    errorMessage("Authentication error!")
  } else {
    errorMessage("Can't retrieve initial data")
  }
}

export const deleteLead = async (id, callback) => {
  let res = await fetch(`/api/leads/${id}/`, {
    method: "DELETE",
  })
  if (res.status == 204) {
    callback(id)
    successMessage("Lead Deleted!")
  } else {
    errorMessage("Error deleting element...")
  }
}

export const addLead = async (lead, callback) => {
  let res = await fetch("/api/leads/", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(lead),
  })
  if (res.status == 201) {
    callback()
    successMessage("Lead Added!")
  } else {
    let err = await res.json()
    let msg = err.name
      ? `Name: ${err.name.join()}`
      : err.email
      ? `Email: ${err.email.join()}!`
      : err.message
      ? `Message: ${err.message.join()}!`
      : "Erro Não identificado"

    errorMessage(msg)
    return false
  }
}
